package com.example.a20220518_namanvishnoi_nycschools.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

public class SchoolsViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    SchoolsRepository schoolsRepository;

    @InjectMocks
    SchoolsViewModel schoolsViewModel = new SchoolsViewModel();

    private Single<List<SchoolModel>> testSingle;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        setUpRxSchedulers();
    }

    @Test
    public void getSchoolsSuccess() {
        //Arrange
        SchoolModel schoolModel = new SchoolModel(
                "123",
                "St. Mary's",
                "1 Queens Dr",
                "NYC",
                "NY",
                "10001",
                "123456789",
                "abc@gmail.com",
                "www.school.com");
        ArrayList<SchoolModel> schools = new ArrayList<>();
        schools.add(schoolModel);
        testSingle = Single.just(schools);
        Mockito.when(schoolsRepository.getSchoolsSortedByName()).thenReturn(testSingle);

        //Act
        schoolsViewModel.refresh();

        //Assert
        Assert.assertEquals(1, schoolsViewModel.schools.getValue().size());
        Assert.assertEquals(false, schoolsViewModel.hasErrorOccurred.getValue());
        Assert.assertEquals(false, schoolsViewModel.isLoading.getValue());
    }

    @Test
    public void getSchoolsError() {
        //Arrange
        testSingle = Single.error(new Throwable());
        Mockito.when(schoolsRepository.getSchoolsSortedByName()).thenReturn(testSingle);

        //Act
        schoolsViewModel.refresh();

        //Assert
        Assert.assertEquals(true, schoolsViewModel.hasErrorOccurred.getValue());
        Assert.assertEquals(false, schoolsViewModel.isLoading.getValue());
    }

    private void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run, true);
            }
        };

        RxJavaPlugins.setInitNewThreadSchedulerHandler(schedulerCallable -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> immediate);

    }
}
