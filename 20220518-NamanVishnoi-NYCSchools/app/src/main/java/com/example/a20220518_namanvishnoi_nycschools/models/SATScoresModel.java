package com.example.a20220518_namanvishnoi_nycschools.models;

import com.google.gson.annotations.SerializedName;

public class SATScoresModel {

    @SerializedName("dbn")
    String schoolID;

    @SerializedName("sat_critical_reading_avg_score")
    String averageCriticalReadingScore;

    @SerializedName("sat_math_avg_score")
    String averageMathScore;

    @SerializedName("sat_writing_avg_score")
    String averageWritingScore;

    public SATScoresModel(String schoolID, String averageCriticalReadingScore, String averageMathScore, String averageWritingScore) {
        this.schoolID = schoolID;
        this.averageCriticalReadingScore = averageCriticalReadingScore;
        this.averageMathScore = averageMathScore;
        this.averageWritingScore = averageWritingScore;
    }

    public String getAverageCriticalReadingScore() {
        return averageCriticalReadingScore;
    }

    public String getAverageMathScore() {
        return averageMathScore;
    }

    public String getAverageWritingScore() {
        return averageWritingScore;
    }
}
