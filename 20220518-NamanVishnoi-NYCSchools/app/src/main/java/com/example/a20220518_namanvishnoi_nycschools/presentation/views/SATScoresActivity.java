package com.example.a20220518_namanvishnoi_nycschools.presentation.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import com.example.a20220518_namanvishnoi_nycschools.R;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;
import com.example.a20220518_namanvishnoi_nycschools.utils.StringUtils;
import com.example.a20220518_namanvishnoi_nycschools.viewmodels.SATScoresViewModel;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SATScoresActivity extends AppCompatActivity {

    @BindView(R.id.schoolName)
    TextView schoolName;

    @BindView(R.id.schoolAddress)
    TextView schoolAddress;

    @BindView(R.id.schoolPhone)
    TextView schoolPhone;

    @BindView(R.id.schoolWebsite)
    TextView schoolWebsite;

    @BindView(R.id.schoolEmail)
    TextView schoolEmail;

    @BindView(R.id.schoolMathScore)
    TextView schoolMathScore;

    @BindView(R.id.schoolReadingScore)
    TextView schoolReadingScore;

    @BindView(R.id.schoolWritingScore)
    TextView schoolWritingScore;

    private SATScoresViewModel satScoresViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sat_scores);

        ButterKnife.bind(this);

        Intent intent = getIntent();
        String schoolJson = intent.getStringExtra(MainActivity.EXTRA_SCHOOL_MODEL);
        SchoolModel schoolModel = new Gson().fromJson(schoolJson, SchoolModel.class);

        satScoresViewModel = ViewModelProviders.of(this).get(SATScoresViewModel.class);
        bindData(schoolModel);

        observerViewModel();
    }

    private void bindData(SchoolModel schoolModel) {

        final String address = String.format("%s, %s %s %s", schoolModel.getAddressLine1(), schoolModel.getCity(), schoolModel.getStateCode(), schoolModel.getZip());
        schoolName.setText(schoolModel.getName());
        schoolAddress.setText(address);
        schoolPhone.setText(schoolModel.getPhoneNumber());
        schoolWebsite.setText(schoolModel.getWebsite());
        schoolEmail.setText(schoolModel.getEmail());

        //Call second api
        satScoresViewModel.fetchSATScores(schoolModel.getSchoolID());
    }

    private void observerViewModel() {
        satScoresViewModel.satScores.observe(this, satScoresModel -> {
            if (satScoresModel != null) {
                schoolMathScore.setText(getScoreText(satScoresModel.getAverageMathScore()));
                schoolReadingScore.setText(getScoreText(satScoresModel.getAverageCriticalReadingScore()));
                schoolWritingScore.setText(getScoreText(satScoresModel.getAverageWritingScore()));
            }
        });
    }

    private String getScoreText(String input) {
        return StringUtils.isValidNumber(input) ? input : getString(R.string.not_available);
    }
}