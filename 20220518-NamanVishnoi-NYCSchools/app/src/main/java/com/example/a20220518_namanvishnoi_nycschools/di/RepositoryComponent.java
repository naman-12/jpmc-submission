package com.example.a20220518_namanvishnoi_nycschools.di;

import com.example.a20220518_namanvishnoi_nycschools.viewmodels.SATScoresViewModel;
import com.example.a20220518_namanvishnoi_nycschools.viewmodels.SchoolsViewModel;

import dagger.Component;

@Component(modules = {RepositoryModule.class})
public interface RepositoryComponent {

    void inject(SchoolsViewModel schoolsViewModel);

    void inject(SATScoresViewModel satScoresViewModel);

}
