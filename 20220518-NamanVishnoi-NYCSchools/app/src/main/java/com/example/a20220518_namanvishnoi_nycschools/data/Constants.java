package com.example.a20220518_namanvishnoi_nycschools.data;

public final class Constants {

    private Constants() {
    }

    public static final String SCHOOLS_BASE_URL = "https://data.cityofnewyork.us";
    public static final String GET_SCHOOLS_URL = "/resource/s3k6-pzi2.json";
    public static final String GET_SORTED_SCHOOLS_URL = "/resource/s3k6-pzi2.json?$order=school_name";
    public static final String GET_SAT_SCORE_URL = "/resource/f9bf-2cp4.json";
}