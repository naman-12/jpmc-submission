package com.example.a20220518_namanvishnoi_nycschools.data.api;

import static com.example.a20220518_namanvishnoi_nycschools.data.Constants.GET_SAT_SCORE_URL;
import static com.example.a20220518_namanvishnoi_nycschools.data.Constants.GET_SCHOOLS_URL;
import static com.example.a20220518_namanvishnoi_nycschools.data.Constants.GET_SORTED_SCHOOLS_URL;

import com.example.a20220518_namanvishnoi_nycschools.models.SATScoresModel;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface SchoolsApi {

    @GET(GET_SCHOOLS_URL)
    Single<List<SchoolModel>> getSchools();

    //Should do sorting at the backend rather than front end
    @GET(GET_SORTED_SCHOOLS_URL)
    Single<List<SchoolModel>> getSchoolsSortedByName();

    @GET(GET_SAT_SCORE_URL)
    Single<List<SATScoresModel>> getSchoolSATScores(@Query("dbn") String schoolID);

}
