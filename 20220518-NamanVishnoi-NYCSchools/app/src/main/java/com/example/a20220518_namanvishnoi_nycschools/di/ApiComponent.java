package com.example.a20220518_namanvishnoi_nycschools.di;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;

import dagger.Component;

@Component(modules = {ApiModule.class})
public interface ApiComponent {

    void inject(SchoolsRepository repository);

}
