package com.example.a20220518_namanvishnoi_nycschools.presentation.views;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.a20220518_namanvishnoi_nycschools.R;
import com.example.a20220518_namanvishnoi_nycschools.presentation.adapters.SchoolsListAdapter;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;
import com.example.a20220518_namanvishnoi_nycschools.viewmodels.SchoolsViewModel;
import com.google.gson.Gson;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_SCHOOL_MODEL = "com.example.myfirstapp.SCHOOL";

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    @BindView(R.id.schoolsList)
    RecyclerView schoolsList;

    @BindView(R.id.error_text)
    TextView errorText;

    @BindView(R.id.loadingIndicator)
    ProgressBar loadingIndicator;

    private SchoolsViewModel schoolsViewModel;
    private SchoolsListAdapter schoolsListAdapter = new SchoolsListAdapter(new ArrayList<>(),
            this::navigateToSchoolDetail);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        schoolsViewModel = ViewModelProviders.of(this).get(SchoolsViewModel.class);
        schoolsViewModel.refresh();

        schoolsList.setLayoutManager(new LinearLayoutManager(this));
        schoolsList.setAdapter(schoolsListAdapter);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            schoolsViewModel.refresh();
            swipeRefreshLayout.setRefreshing(false);
        });
        observerViewModel();
    }

    private void observerViewModel() {
        schoolsViewModel.schools.observe(this, schoolModels -> {
            if (schoolModels != null) {
                schoolsList.setVisibility(View.VISIBLE);
                schoolsListAdapter.updateSchools(schoolModels);
            }
        });

        schoolsViewModel.hasErrorOccurred.observe(this, error -> {
            if (error != null) {
                errorText.setVisibility(error ? View.VISIBLE : View.GONE);
            }
        });

        schoolsViewModel.isLoading.observe(this, loading -> {
            if (loading != null) {
                loadingIndicator.setVisibility(loading ? View.VISIBLE : View.GONE);
            }
        });
    }

    public void navigateToSchoolDetail(SchoolModel schoolModel) {
        Intent intent = new Intent(this, SATScoresActivity.class);
        String schoolJson = new Gson().toJson(schoolModel);
        intent.putExtra(EXTRA_SCHOOL_MODEL, schoolJson);
        startActivity(intent);
    }
}