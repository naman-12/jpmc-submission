package com.example.a20220518_namanvishnoi_nycschools.viewmodels;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;
import com.example.a20220518_namanvishnoi_nycschools.models.SATScoresModel;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.android.plugins.RxAndroidPlugins;
import io.reactivex.internal.schedulers.ExecutorScheduler;
import io.reactivex.plugins.RxJavaPlugins;

public class SATScoresViewModelTest {

    @Rule
    public InstantTaskExecutorRule rule = new InstantTaskExecutorRule();

    @Mock
    SchoolsRepository schoolsRepository;

    @InjectMocks
    SATScoresViewModel satScoresViewModel = new SATScoresViewModel();

    private Single<List<SATScoresModel>> testSingle;

    @Before
    public void setUp() {
        MockitoAnnotations.openMocks(this);
        setUpRxSchedulers();
    }

    @Test
    public void fetchSATScores() {
        //Arrange
        String schoolID = "123";
        SATScoresModel satScoresModel = new SATScoresModel("123", "345", "320", "334");
        ArrayList<SATScoresModel> satScoresList = new ArrayList<>();
        satScoresList.add(satScoresModel);
        testSingle = Single.just(satScoresList);
        Mockito.when(schoolsRepository.getSchoolSATScores(schoolID)).thenReturn(testSingle);

        //Act
        satScoresViewModel.fetchSATScores(schoolID);

        //Assert
        Assert.assertEquals("345", satScoresViewModel.satScores.getValue().getAverageCriticalReadingScore());
        Assert.assertEquals("320", satScoresViewModel.satScores.getValue().getAverageMathScore());
        Assert.assertEquals("334", satScoresViewModel.satScores.getValue().getAverageWritingScore());
    }

    private void setUpRxSchedulers() {
        Scheduler immediate = new Scheduler() {
            @Override
            public Worker createWorker() {
                return new ExecutorScheduler.ExecutorWorker(Runnable::run, true);
            }
        };

        RxJavaPlugins.setInitNewThreadSchedulerHandler(schedulerCallable -> immediate);
        RxAndroidPlugins.setInitMainThreadSchedulerHandler(schedulerCallable -> immediate);

    }

}
