package com.example.a20220518_namanvishnoi_nycschools.data.repositories;

import com.example.a20220518_namanvishnoi_nycschools.data.api.SchoolsApi;
import com.example.a20220518_namanvishnoi_nycschools.di.DaggerApiComponent;
import com.example.a20220518_namanvishnoi_nycschools.models.SATScoresModel;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;

public class SchoolsRepository {

    private static SchoolsRepository instance;

    @Inject
    public SchoolsApi api;

    private SchoolsRepository() {
        DaggerApiComponent.create().inject(this);
    }

    public static SchoolsRepository getInstance() {
        if (instance == null) {
            instance = new SchoolsRepository();
        }
        return instance;
    }

    public Single<List<SchoolModel>> getSchools() {
        return api.getSchools();
    }

    public Single<List<SchoolModel>> getSchoolsSortedByName() {
        return api.getSchoolsSortedByName();
    }

    public Single<List<SATScoresModel>> getSchoolSATScores(String schoolID) {
        return api.getSchoolSATScores(schoolID);
    }
}
