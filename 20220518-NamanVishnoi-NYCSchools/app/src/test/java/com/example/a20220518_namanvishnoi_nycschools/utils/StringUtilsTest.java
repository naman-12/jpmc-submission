package com.example.a20220518_namanvishnoi_nycschools.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

    @Test
    public void validInteger() {
        Assert.assertTrue(StringUtils.isValidNumber("345"));
        Assert.assertTrue(StringUtils.isValidNumber("0"));
    }

    @Test
    public void invalidInteger() {
        Assert.assertFalse(StringUtils.isValidNumber("s"));
        Assert.assertFalse(StringUtils.isValidNumber(""));
        Assert.assertFalse(StringUtils.isValidNumber(null));
    }
}
