package com.example.a20220518_namanvishnoi_nycschools.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;
import com.example.a20220518_namanvishnoi_nycschools.di.DaggerRepositoryComponent;
import com.example.a20220518_namanvishnoi_nycschools.models.SATScoresModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SATScoresViewModel extends ViewModel {

    public MutableLiveData<SATScoresModel> satScores = new MutableLiveData<>();

    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    public SchoolsRepository schoolsRepository;

    public SATScoresViewModel() {
        super();
        DaggerRepositoryComponent.create().inject(this);
    }

    public void fetchSATScores(String schoolID) {
        disposable.add(schoolsRepository.getSchoolSATScores(schoolID)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<SATScoresModel>>() {

                    @Override
                    public void onSuccess(List<SATScoresModel> satScoresModelList) {
                        if (!satScoresModelList.isEmpty()) {
                            satScores.setValue(satScoresModelList.get(0)); //Ideally, the satScore api should only return a single element since a school cannot have two diff set of SAT scores.
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                }));
    }


    @Override
    protected void onCleared() {
        disposable.clear();
    }

}
