package com.example.a20220518_namanvishnoi_nycschools.presentation.customListeners;

import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

public interface SchoolViewClickListener {
    void onClick(SchoolModel school);
}
