package com.example.a20220518_namanvishnoi_nycschools.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;
import com.example.a20220518_namanvishnoi_nycschools.di.DaggerRepositoryComponent;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class SchoolsViewModel extends ViewModel {

    public MutableLiveData<List<SchoolModel>> schools = new MutableLiveData<>();
    public MutableLiveData<Boolean> hasErrorOccurred = new MutableLiveData<>();
    public MutableLiveData<Boolean> isLoading = new MutableLiveData<>();

    public void refresh() {
        fetchSchools();
    }

    private CompositeDisposable disposable = new CompositeDisposable();

    @Inject
    public SchoolsRepository schoolsRepository;

    public SchoolsViewModel() {
        super();
        DaggerRepositoryComponent.create().inject(this);
    }

    private void fetchSchools() {
        isLoading.setValue(true);
        disposable.add(schoolsRepository.getSchoolsSortedByName()
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<SchoolModel>>() {

                    @Override
                    public void onSuccess(List<SchoolModel> schoolModels) {
                        schools.setValue(schoolModels);
                        hasErrorOccurred.setValue(false);
                        isLoading.setValue(false);
                    }

                    @Override
                    public void onError(Throwable e) {
                        hasErrorOccurred.setValue(true);
                        isLoading.setValue(false);
                        e.printStackTrace();
                    }
                }));
    }

    @Override
    protected void onCleared() {
        disposable.clear();
    }
}
