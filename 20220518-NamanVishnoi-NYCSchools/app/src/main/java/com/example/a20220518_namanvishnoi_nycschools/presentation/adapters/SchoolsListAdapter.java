package com.example.a20220518_namanvishnoi_nycschools.presentation.adapters;

import static androidx.recyclerview.widget.RecyclerView.NO_POSITION;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.a20220518_namanvishnoi_nycschools.R;
import com.example.a20220518_namanvishnoi_nycschools.presentation.customListeners.SchoolViewClickListener;
import com.example.a20220518_namanvishnoi_nycschools.models.SchoolModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SchoolsListAdapter extends RecyclerView.Adapter<SchoolsListAdapter.SchoolViewHolder> {

    private List<SchoolModel> schools;
    private SchoolViewClickListener onClickListener;

    public SchoolsListAdapter(List<SchoolModel> schools, SchoolViewClickListener onClickListener) {
        this.schools = schools;
        this.onClickListener = onClickListener;
    }

    public void updateSchools(List<SchoolModel> newSchools) {
        schools.clear();
        schools.addAll(newSchools);
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public SchoolViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_school, parent, false);
        final SchoolViewHolder viewHolder = new SchoolViewHolder(view);

        viewHolder.itemView.setOnClickListener(v -> {
            int position = viewHolder.getAdapterPosition();
            if (position != NO_POSITION) {
                final SchoolModel schoolModel = schools.get(position);
                onClickListener.onClick(schoolModel);
            }
        });

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull SchoolViewHolder holder, int position) {
        holder.bind(schools.get(position));
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }

    class SchoolViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.schoolName)
        TextView schoolName;

        @BindView(R.id.schoolCity)
        TextView schoolCity;

        public SchoolViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        void bind(SchoolModel model) {
            schoolName.setText(model.getName());
            schoolCity.setText(model.getCity());
        }

    }
}
