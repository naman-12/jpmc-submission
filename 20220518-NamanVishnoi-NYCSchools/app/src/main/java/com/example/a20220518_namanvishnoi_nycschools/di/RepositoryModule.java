package com.example.a20220518_namanvishnoi_nycschools.di;

import com.example.a20220518_namanvishnoi_nycschools.data.repositories.SchoolsRepository;

import dagger.Module;
import dagger.Provides;

@Module
public class RepositoryModule {

    @Provides
    public SchoolsRepository provideSchoolsRepository() {
        return SchoolsRepository.getInstance();
    }

}
