package com.example.a20220518_namanvishnoi_nycschools.models;

import com.google.gson.annotations.SerializedName;

public class SchoolModel {

    @SerializedName("dbn")
    String schoolID;

    @SerializedName("school_name")
    String name;

    @SerializedName("primary_address_line_1")
    String addressLine1;

    @SerializedName("city")
    String city;

    @SerializedName("state_code")
    String stateCode;

    @SerializedName("zip")
    String zip;

    @SerializedName("phone_number")
    String phoneNumber;

    @SerializedName("school_email")
    String email;

    @SerializedName("website")
    String website;

    public SchoolModel(String schoolID, String name, String addressLine1, String city, String stateCode, String zip, String phoneNumber, String email, String website) {
        this.schoolID = schoolID;
        this.name = name;
        this.addressLine1 = addressLine1;
        this.city = city;
        this.stateCode = stateCode;
        this.zip = zip;
        this.phoneNumber = phoneNumber;
        this.email = email;
        this.website = website;
    }

    public String getWebsite() {
        return website;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public String getCity() {
        return city;
    }

    public String getStateCode() {
        return stateCode;
    }

    public String getZip() {
        return zip;
    }

    public String getSchoolID() {
        return schoolID;
    }

    public String getName() {
        return name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public String getEmail() {
        return email;
    }
}
