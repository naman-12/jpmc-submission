package com.example.a20220518_namanvishnoi_nycschools.utils;

public final class StringUtils {

    private StringUtils() {

    }

    public static boolean isValidNumber(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
