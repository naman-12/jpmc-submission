package com.example.a20220518_namanvishnoi_nycschools.di;

import com.example.a20220518_namanvishnoi_nycschools.data.Constants;
import com.example.a20220518_namanvishnoi_nycschools.data.api.SchoolsApi;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    public SchoolsApi provideSchoolsApi() {
        return new Retrofit.Builder()
                .baseUrl(Constants.SCHOOLS_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(SchoolsApi.class);
    }
}
